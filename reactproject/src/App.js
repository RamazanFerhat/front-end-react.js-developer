import './main.scss';
import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./components/Home";
import Contact from "./components/Contact";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { title: "VAT Checker" }
  }
  render() {
    return (
      <HashRouter>
        <div>
          <div className="headerInfo">
              <p>support@despatchcloud.com | 01377 455 180 </p>
          </div>
          <div className="header">
            <ul>
              <li><NavLink to="/contact">Contact</NavLink></li>
              <li><NavLink to="/">Home</NavLink></li>
            </ul>
          </div>
          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/contact" component={Contact} />
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;