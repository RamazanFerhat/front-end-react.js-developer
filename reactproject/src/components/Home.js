import React, { Component } from "react";
import imagee from "./homeimg.jpg";
import axios from "axios";
import '../main.scss';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trader_name: '',
      trader_address: '',
      valid: false,
      vat_number: '',
      explanation: '',
      color: '',
      showResult: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleClick() {
    this.getVatCheck(this.state.value);
  }
  getVatCheck(vatChechk) {
    const vatNumber = vatChechk;
    axios.get('https://vat.anilcancakir.com/api/vat/check?vat_number=' + vatNumber)
      .then(response => {
        if (response.data && response.data.data && response.data.data.valid === true)
          this.getResult(response.data.data);
        else if (response.data && response.data.data && response.data.data.valid === false) {
          this.setState({
            explanation: "The number is invalid on the specified date",
            color: "#e63030",
            valid: false,
            showResult: true,
          });
        }
      })
      .catch(error => {
        
      });
  }
  getResult(data) {
    this.setState({
      trader_name: data.trader_name,
      trader_address: data.trader_address,
      explanation: "The number is valid on the specified date",
      color: "#46d6e0",
      showResult: true,
      valid: data.valid,
    })
  }
  render() {
    return (
      <div>
        <h2>VAT Checker</h2>
        <div className="homeContact">
          <div className="divLeft">
            <h2> Check the Value Added Tax Number</h2>
            <p> VAT Checker allows you to check the validity
              of a VAT number prior to applying the 0% rate when
              selling goods or services to EU countries.
                  </p>
          </div>
          <div className="divRight">
            <img src={imagee} />
          </div>
        </div>
        <div>
          <div>
            <div className="buttonFrom">
              <h1 style={{ textAlign: "center" }}>VAT Checker</h1>
            </div>
            <div className="input-group mb-3 vatCeheckInput">
              <input type="text" style={{ width: "350px", paddingLeft: "50px" }} className="CeheckInput" value={this.state.value} onChange={this.handleChange} aria-describedby="basic-addon2" />
              <div className="input-group-append">
                <button style={{ backgroundColor: "rgb(9, 177, 255)", borderRadius: "40px" }} className="btn btn-outline-default" onClick={this.handleClick} type="button">Check</button>
              </div>
            </div>
            {
              this.state.showResult === true ?
                <div style={{ backgroundColor: this.state.color }} className="result">
                  <p>{this.state.explanation}</p>
                  {
                    this.state.valid === true ?
                      <div>
                        <label>Trader Name: {this.state.trader_name}</label>
                        <br></br>
                        <label>Trader Address: {this.state.trader_address}</label>
                      </div>
                      : null
                  }
                </div>
                : null
            }

          </div>
        </div>
      </div>
    );
  }
}

export default Home;